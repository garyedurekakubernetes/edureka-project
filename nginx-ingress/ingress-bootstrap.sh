#!/bin/bash
###############################
# bootstraping nginx ingress  #
#  controller and services    #
###############################

# create basic resources for nginx ingress

kubectl apply -f common/service-account.yaml
kubectl apply -f common/nginx-ingress-default-secret.yaml
kubectl apply -f common/nginx-config.yaml
kubectl apply -f rbac/nginx-ingress-rbac.yaml
kubectl apply -f daemonset/nginx-ingress.yaml

# setup loadbalancer for nginx ingress

kubectl apply -f service/loadbalancer-aws-elb.yaml

# get loadbalancer address and store in a variable for reference later on.

getlbaddress="kubectl describe svc nginx-ingress --namespace=nginx-ingress| grep 'LoadBalancer Ingress:' "
eval $getlbaddress
Loadbalancer=$(eval $getlbaddress)
echo $Loadbalancer | awk '{split($0,i,":"); print i[2]}'